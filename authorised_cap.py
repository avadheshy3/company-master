# this will show the information of all the autherised cap
import csv
import matplotlib.pylab as plt


def read_compnay_data():
    company_car_count = [0, 0, 0, 0, 0]
    with open("Maharashtra.csv", 'r', encoding="unicode_escape") as csvfile:
        reader_variable = csv.DictReader(csvfile)
        for company_row in reader_variable:
            capital = float(company_row['AUTHORIZED_CAP'])
            if(capital <= 100000):
                company_car_count[0] += 1
            elif capital <= 1000000:
                company_car_count[1] += 1
            elif capital <= 10000000:
                company_car_count[2] += 1
            elif capital <= 100000000:
                company_car_count[3] += 1
            else:
                company_car_count[4] += 1
    return company_car_count


def display_company_capital(capital):
    Range = ["<=1L", "<=10L", "<=1Cr", "<=10Cr", ">10Cr"]

    plt.bar(Range, capital)
    plt.xlabel('range')
    plt.ylabel('number of cars in that range')
    plt.show()
# This is completed


if __name__ == '__main__':
    capital = read_compnay_data()
    display_company_capital(capital)
    # this is completed
