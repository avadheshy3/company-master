import csv
import matplotlib.pylab as plt
from collections import Counter


def read_data():
    year_wise_data = {}
    with open("Maharashtra.csv", 'r', encoding="unicode_escape") as csvfile:
        reader_variable = csv.DictReader(csvfile)
        for company in reader_variable:
            year = (company['DATE_OF_REGISTRATION']).strip()
            if len(year) == 8:
                day, month, year = year.split('-')
                if '10' < year <= '20':
                    active = company['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
                    activity = active
                    year = "20" + year
                    if year in year_wise_data.keys():
                        if activity in year_wise_data[year].keys():
                            year_wise_data[year][activity] += 1
                        else:
                            year_wise_data[year][activity] = 1
                    else:
                        year_wise_data[year] = {activity: 1}
    top_five_company_per_year = []
    for year in sorted(year_wise_data.keys()):
        top_five = dict(Counter(year_wise_data[year]).most_common(5))
        five = [i for i in top_five.values()]
        top_five_company_per_year.append(five)

    data = [[0 for i in range(10)] for j in range(5)]
    for i in range(len(top_five_company_per_year)):
        for j in range(len(top_five_company_per_year[i])):
            data[j][i] = top_five_company_per_year[i][j]

    return data


def display_group_data(data):
    x = [i for i in range(10)]
    width = 0.05
    z = 0.2
    for i in range(5):
        plt.bar(list(map(lambda y: y - z, x)), data[i], width)
        z -= 0.1
    plt.xlabel("year from 2011 to 2020")
    plt.ylabel("number of times a activity happens in one year")
    plt.legend(['first', 'second', 'third', 'fourth', 'fifth'])
    plt.xticks(x, [i for i in range(2011, 2021)])
    plt.show()


if __name__ == '__main__':
    data = read_data()
    display_group_data(data)
