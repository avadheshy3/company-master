import csv
import matplotlib.pylab as plt


def read_data():
    year_data = {}
    # reading data from csv file
    with open("Maharashtra.csv", 'r', encoding="unicode_escape") as csvfile:
        reader_variable = csv.DictReader(csvfile)
        for company in reader_variable:
            years = (company['DATE_OF_REGISTRATION']).strip()
            if len(years) > 4:
                day, month, year = years.split('-')
                if(len(years) == 8):
                    year = int(year)
                    if(year >= 0 and year <= 9):
                        year = "200" + str(year)
                    elif(year >= 10 and year <= 20):
                        year = "20" + str(year)
                    else:
                        year = "19" + str(year)
            if year in year_data.keys():
                year_data[year] += 1
            else:
                year_data[year] = 1
    year = []
    numberOfCar = []
    for i in sorted(year_data.keys()):
        if(year_data[i] > 500):
            year.append(i)
            numberOfCar.append(year_data[i])
    return [year, numberOfCar]


def display_registration_by_year(year, numberOfRegistration):
    plt.barh(year, numberOfRegistration)
    plt.xlabel("Number of registration per year")
    plt.ylabel("year")
    plt.show()
# completed


if __name__ == '__main__':
    registrationData = read_data()
    display_registration_by_year(registrationData[0], registrationData[1])
