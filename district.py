import csv
import matplotlib.pylab as plt
from pincode import Pincode


def read_data_company():
    District = {}
    with open("Maharashtra.csv", 'r', encoding="unicode_escape") as csvfile:
        reader_variable = csv.DictReader(csvfile)
        for company in reader_variable:
            Year = (company['DATE_OF_REGISTRATION']).strip()
            if len(Year) == 8:
                day, month, year = Year.split('-')
                if(year == "15"):
                    adress = company['Registered_Office_Address'].split()
                    zip = adress[-1]
                    if(zip.isdigit()):
                        if zip in Pincode.keys():
                            if Pincode[zip] in District.keys():
                                District[Pincode[zip]] += 1
                            else:
                                District[Pincode[zip]] = 1
    return District


def display_registration_by_district(district, nubmerOfCars):
    plt.bar(district, nubmerOfCars)
    plt.xlabel("Name of the districts of maharashtra")
    plt.ylabel("Number of registration in year 2015")
    plt.xticks(rotation=10)
    plt.show()
# completed


if __name__ == '__main__':
    data = read_data_company()
    display_registration_by_district(data.keys(), data.values())
    # completed
